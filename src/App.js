import React from 'react';
import './App.css';
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import 'bootstrap/dist/css/bootstrap.min.css';
import Form from 'react-bootstrap/Form'
import Table from 'react-bootstrap/Table'

function App() {
  
  return (
    <div class="container-fluid">
      <div class="container">
        <div class="box1">
          <Card>
            <Card.Img variant="top" src="logo192.png" />
            <Card.Body>
              <Form.Group controlId="formBasicEmail">
                <Form.Control type="text" />
                <Form.Text className="text-muted">
                </Form.Text>
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Control type="text" />
              </Form.Group>
              <Form.Group controlId="exampleForm.ControlSelect1" >
                <Form.Control as="select" name="cal" >
                  <option value="+">+ Plus</option>
                  <option value="-">- Subtrac</option>
                  <option value="*">* Multiply</option>
                  <option value="/">/ Division</option>
                  <option value="%">% Module</option>
                </Form.Control>
              </Form.Group>
              <Button variant="primary"type="button"  onClick={
                            (e)=>this.onCalculate(e)
                        }>Calculate</Button>
            </Card.Body>
          </Card>
        </div>
        <div class="box2">
        <Form.Label class="text">Result History</Form.Label>
          <Table  striped bordered hover>
            <tbody>
              <tr > 
                <td></td>
              </tr>
            </tbody>
          </Table>
        </div>
      </div>
    </div>
  );
}
export default App;
